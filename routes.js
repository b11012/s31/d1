const http = require("http");

// Creates a variable 'port' to store the port number
const port = 4000;

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((req, res) => {

	// Accessing the 'greeting' route returns a message of 'Hi I am from greeting'
	if(req.url === '/greeting'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Hi I am from greeting')
	    
	// Accessing the 'homepage' route returns a message of 'This is the homepage'
	} else if(req.url === '/homepage'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('This is the homepage')

	// All other routes will return a message of 'Page not available'
	} else {
		res.writeHead(404, {'Content-Type' : 'text/plain'})
		res.end('Page not available')
	}

})
// Uses the "server" and "port" variables created above.
server.listen(port);

// When server is running, console will print the message:
console.log(`Server is now connected at localhost:${port}.`)
